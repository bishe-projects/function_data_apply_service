package po

type FunctionDataApply struct {
	ID          int64
	ApplicantId int64
	FunctionId  int64
	BusinessId  int64
	Status      int32
}

type FunctionDataApplyResult struct {
	ID            int64
	ApplicantId   int64
	ApplicantName string
	FunctionId    int64
	FunctionName  string
	BusinessId    int64
	BusinessName  string
	Status        int32
}
