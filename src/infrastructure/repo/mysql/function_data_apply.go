package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/repo/po"
)

var FunctionDataApplyMySQLDao = new(FunctionDataApply)

type FunctionDataApply struct{}

const FunctionDataApplyTable = "function_data_apply"

func (r *FunctionDataApply) CreateApply(apply *po.FunctionDataApply) error {
	err := db.Table(FunctionDataApplyTable).Create(&apply).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *FunctionDataApply) GetApplyByID(applyId int64) (*po.FunctionDataApply, error) {
	var apply *po.FunctionDataApply
	result := db.Table(FunctionDataApplyTable).First(&apply, applyId)
	return apply, result.Error
}

func (r *FunctionDataApply) GetApplyList(applicantId, functionId, businessId *int64, status *int32) ([]*po.FunctionDataApplyResult, error) {
	var applyList []*po.FunctionDataApplyResult
	d := db
	d = d.
		Table(FunctionDataApplyTable).
		Select("function_data_apply.id, function_data_apply.applicant_id, user.name as applicant_name, function_data_apply.function_id, function.name as function_name, function_data_apply.business_id, business.name as business_name, function_data_apply.status").
		Joins("join user on function_data_apply.applicant_id = user.id").
		Joins("join `function` on function_data_apply.function_id = function.id").
		Joins("join business on function_data_apply.business_id = business.id")
	if applicantId != nil {
		d = d.Where("applicant_id = ?", *applicantId)
	}
	if functionId != nil {
		d = d.Where("function_id = ?", *functionId)
	}
	if businessId != nil {
		d = d.Where("business_id = ?", *businessId)
	}
	if status != nil {
		d = d.Where("status = ?", *status)
	}
	result := d.Order("status").Scan(&applyList)
	return applyList, result.Error
}

func (r *FunctionDataApply) UpdateApply(apply *po.FunctionDataApply, values map[string]interface{}) error {
	return db.Table(FunctionDataApplyTable).Model(&apply).Updates(values).Error
}
