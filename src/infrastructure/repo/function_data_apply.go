package repo

import (
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/repo/po"
)

type FunctionDataApplyDao interface {
	CreateApply(*po.FunctionDataApply) error
	GetApplyByID(int64) (*po.FunctionDataApply, error)
	GetApplyList(*int64, *int64, *int64, *int32) ([]*po.FunctionDataApplyResult, error)
	UpdateApply(*po.FunctionDataApply, map[string]interface{}) error
}

var FunctionDataApplyMySQLRepo = NewFunctionDataApplyRepo(mysql.FunctionDataApplyMySQLDao)

type FunctionDataApply struct {
	FunctionDataApplyDao FunctionDataApplyDao
}

func NewFunctionDataApplyRepo(functionDataApplyDao FunctionDataApplyDao) *FunctionDataApply {
	return &FunctionDataApply{
		FunctionDataApplyDao: functionDataApplyDao,
	}
}
