package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_auth/functiondataauthservice"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user/userservice"
)

var (
	UserClient             = userservice.MustNewClient("user_service", client.WithHostPorts("0.0.0.0:8881"), client.WithTransportProtocol(transport.TTHeaderFramed))
	FunctionDataAuthClient = functiondataauthservice.MustNewClient("function_data_auth_service", client.WithHostPorts("0.0.0.0:8884"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
