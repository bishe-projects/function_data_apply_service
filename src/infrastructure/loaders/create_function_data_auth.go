package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_auth"
)

type CreateFunctionDataAuthLoader struct {
	loader.CommonLoader
	// req
	ctx        context.Context
	uid        int64
	functionId int64
	businessId int64
}

func (l *CreateFunctionDataAuthLoader) Load() error {
	resp, err := client.FunctionDataAuthClient.CreateAuth(l.ctx, l.newCreateAuthReq())
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[CreateFunctionDataAuthLoader] create auth failed: uid=%d, functionId=%v, businessId=%d, err=%s", l.uid, l.functionId, l.businessId, err)
		l.SetError(err)
	}
	return err
}

func (l *CreateFunctionDataAuthLoader) newCreateAuthReq() *function_data_auth.CreateAuthReq {
	return &function_data_auth.CreateAuthReq{
		Uid:        l.uid,
		FunctionId: l.functionId,
		BusinessId: l.businessId,
	}
}

func NewCreateFunctionDataAuthLoader(ctx context.Context, uid, functionId, businessId int64) *CreateFunctionDataAuthLoader {
	return &CreateFunctionDataAuthLoader{
		ctx:        ctx,
		uid:        uid,
		functionId: functionId,
		businessId: businessId,
	}
}
