package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
)

type HasUserRoleLoader struct {
	loader.CommonLoader
	// req
	ctx        context.Context
	uid        int64
	roleId     int64
	functionId *int64
	// resp
	Has bool
}

func (l *HasUserRoleLoader) Load() error {
	resp, err := client.UserClient.HasUserRole(l.ctx, l.newHasUserRoleReq())
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[HasUserRoleLoader] get has user role failed: uid=%d, roleId=%d, functionId=%v, err=%s", l.uid, l.roleId, l.functionId, err)
		l.SetError(err)
		return err
	}
	l.Has = resp.Has
	return nil
}

func (l *HasUserRoleLoader) newHasUserRoleReq() *user.HasUserRoleReq {
	return &user.HasUserRoleReq{
		Uid:        l.uid,
		RoleId:     l.roleId,
		FunctionId: l.functionId,
	}
}

func NewHasUserRoleLoader(ctx context.Context, uid, roleId int64, functionId *int64) *HasUserRoleLoader {
	return &HasUserRoleLoader{
		ctx:        ctx,
		uid:        uid,
		roleId:     roleId,
		functionId: functionId,
	}
}
