package biz_error

import "gitlab.com/bishe-projects/common_utils/business_error"

var (
	CreateApplyErr    = business_error.NewBusinessError("create apply failed", -10001)
	GetApplyByIDErr   = business_error.NewBusinessError("get apply by id failed", -10002)
	ChangeStatusErr   = business_error.NewBusinessError("apply change status failed", -10003)
	GetApplyListErr   = business_error.NewBusinessError("get apply list failed", -10004)
	ApplyExistsErr    = business_error.NewBusinessError("the application is under review", -10005)
	HasUserRoleErr    = business_error.NewBusinessError("get user role failed", -10006)
	ApplyNotExistsErr = business_error.NewBusinessError("apply not exists", -10007)
	ApplyStatusErr    = business_error.NewBusinessError("the application is not an expected status", -10008)
	NotMyWithdrawErr  = business_error.NewBusinessError("you are not the applicant", -10009)
	CreateDataAuthErr = business_error.NewBusinessError("create function data auth failed", -10010)
)
