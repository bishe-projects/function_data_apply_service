package service

import (
	"context"
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	common_constant "gitlab.com/bishe-projects/common_utils/constant"
	"gitlab.com/bishe-projects/common_utils/constant/business_constant"
	"gitlab.com/bishe-projects/function_data_apply_service/src/domain/function_data_apply/entity"
	"gitlab.com/bishe-projects/function_data_apply_service/src/domain/function_data_apply/service"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/loaders"
)

var FunctionDataApplyApp = new(FunctionDataApply)

type FunctionDataApply struct{}

func (a *FunctionDataApply) CreateApply(apply *entity.FunctionDataApply) *business_error.BusinessError {
	err := service.FunctionDataApplyDomain.CreateApply(apply)
	if err == business_error.DataExistsErr {
		return biz_error.ApplyExistsErr
	}
	return err
}

func (a *FunctionDataApply) ApproveApply(ctx context.Context, applyId int64) *business_error.BusinessError {
	apply, err := service.FunctionDataApplyDomain.GetApplyByID(applyId)
	if err != nil {
		return err
	}
	if apply == nil {
		return biz_error.ApplyNotExistsErr
	}
	if apply.Status != business_constant.ApplyStatusApplying {
		return biz_error.ApplyStatusErr
	}
	has, err := a.isFunctionAdmin(ctx, apply.FunctionId)
	if err != nil {
		return err
	}
	if !has {
		return business_error.NoAuthErr
	}
	err = service.FunctionDataApplyDomain.ChangeStatus(apply, business_constant.ApplyStatusApproved)
	if err != nil {
		return err
	}
	loader := loaders.NewCreateFunctionDataAuthLoader(ctx, apply.ApplicantId, apply.FunctionId, apply.BusinessId)
	if loaderErr := loader.Load(); loaderErr != nil {
		return biz_error.CreateDataAuthErr
	}
	return nil
}

func (a *FunctionDataApply) RejectApply(ctx context.Context, applyId int64) *business_error.BusinessError {
	apply, err := service.FunctionDataApplyDomain.GetApplyByID(applyId)
	if err != nil {
		return err
	}
	if apply == nil {
		return biz_error.ApplyNotExistsErr
	}
	if apply.Status != business_constant.ApplyStatusApplying {
		return biz_error.ApplyStatusErr
	}
	has, err := a.isFunctionAdmin(ctx, apply.FunctionId)
	if err != nil {
		return err
	}
	if !has {
		return business_error.NoAuthErr
	}
	return service.FunctionDataApplyDomain.ChangeStatus(apply, business_constant.ApplyStatusRejected)
}

func (a *FunctionDataApply) WithdrawApply(ctx context.Context, applyId int64) *business_error.BusinessError {
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		return err
	}
	apply, err := service.FunctionDataApplyDomain.GetApplyByID(applyId)
	if err != nil {
		return err
	}
	if apply == nil {
		return biz_error.ApplyNotExistsErr
	}
	if apply.Status != business_constant.ApplyStatusApplying {
		return biz_error.ApplyStatusErr
	}
	if apply.ApplicantId != uid {
		return biz_error.NotMyWithdrawErr
	}
	return service.FunctionDataApplyDomain.ChangeStatus(apply, business_constant.ApplyStatusWithdraw)
}

func (a *FunctionDataApply) GetApplyList(applicantId, functionId, businessId *int64, status *int32) ([]*entity.FunctionDataApply, *business_error.BusinessError) {
	return service.FunctionDataApplyDomain.GetApplyList(applicantId, functionId, businessId, status)
}

func (a *FunctionDataApply) AdminApplyList(ctx context.Context, functionId int64, status *int32) ([]*entity.FunctionDataApply, *business_error.BusinessError) {
	has, err := a.isFunctionAdmin(ctx, functionId)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, business_error.NoAuthErr
	}
	return service.FunctionDataApplyDomain.GetApplyList(nil, thrift.Int64Ptr(functionId), nil, status)
}

func (a *FunctionDataApply) OwnApplyList(ctx context.Context, functionId int64, status *int32) ([]*entity.FunctionDataApply, *business_error.BusinessError) {
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		return nil, err
	}
	return service.FunctionDataApplyDomain.GetApplyList(thrift.Int64Ptr(uid), thrift.Int64Ptr(functionId), nil, status)
}

func (a *FunctionDataApply) isFunctionAdmin(ctx context.Context, functionId int64) (bool, *business_error.BusinessError) {
	uid, businessErr := common_utils.GetUidFromCtx(ctx)
	if businessErr != nil {
		return false, businessErr
	}
	loader := loaders.NewHasUserRoleLoader(ctx, uid, common_constant.FunctionAdminRoleID, thrift.Int64Ptr(functionId))
	if err := loader.Load(); err != nil {
		return false, biz_error.HasUserRoleErr
	}
	return loader.Has, nil
}
