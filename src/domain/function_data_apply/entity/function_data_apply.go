package entity

import (
	"github.com/apache/thrift/lib/go/thrift"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/constant/business_constant"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/function_data_apply_service/src/infrastructure/repo/po"
	"gorm.io/gorm"
)

type FunctionDataApply struct {
	ID            int64
	ApplicantId   int64
	ApplicantName string
	FunctionId    int64
	FunctionName  string
	BusinessId    int64
	BusinessName  string
	Status        int32
}

func (fda *FunctionDataApply) CreateApply() *business_error.BusinessError {
	// check already created
	applyList, err := repo.FunctionDataApplyMySQLRepo.FunctionDataApplyDao.GetApplyList(thrift.Int64Ptr(fda.ApplicantId), thrift.Int64Ptr(fda.FunctionId), thrift.Int64Ptr(fda.BusinessId), thrift.Int32Ptr(business_constant.ApplyStatusApplying))
	if err != nil {
		klog.Errorf("[FunctionDataApplyAggregate] create apply failed: err=%s", err)
		return biz_error.CreateApplyErr
	}
	if len(applyList) > 0 {
		return business_error.DataExistsErr
	}
	err = repo.FunctionDataApplyMySQLRepo.FunctionDataApplyDao.CreateApply(fda.convertToFunctionDataApplyPO())
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[FunctionDataApplyAggregate] create apply failed: err=%s", err)
		return biz_error.CreateApplyErr
	}
	return nil
}

func (fda *FunctionDataApply) GetApplyByID(applyId int64) *business_error.BusinessError {
	applyPO, err := repo.FunctionDataApplyMySQLRepo.FunctionDataApplyDao.GetApplyByID(applyId)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[FunctionDataApplyAggregate] get apply by id failed: err=%s", err)
		return biz_error.GetApplyByIDErr
	}
	fda.fillApplyFromApplyPO(applyPO)
	return nil
}

func (fda *FunctionDataApply) ChangeStatus(status int32) *business_error.BusinessError {
	err := repo.FunctionDataApplyMySQLRepo.FunctionDataApplyDao.UpdateApply(fda.convertToFunctionDataApplyPO(), map[string]interface{}{"status": status})
	if err != nil {
		klog.Errorf("[FunctionDataApplyAggregate] apply change status failed: err=%s", err)
		return biz_error.ChangeStatusErr
	}
	return nil
}

type FunctionDataApplyList []*FunctionDataApply

func (fdal *FunctionDataApplyList) GetApplyList(applicantId, functionId, businessId *int64, status *int32) *business_error.BusinessError {
	applyPOList, err := repo.FunctionDataApplyMySQLRepo.FunctionDataApplyDao.GetApplyList(applicantId, functionId, businessId, status)
	if err != nil {
		klog.Errorf("[FunctionDataApplyAggregate] get apply list failed: err=%s", err)
		return biz_error.GetApplyListErr
	}
	fdal.fillApplyListFromApplyResultPOList(applyPOList)
	return nil
}

// converter

func (fda *FunctionDataApply) convertToFunctionDataApplyPO() *po.FunctionDataApply {
	return &po.FunctionDataApply{
		ID:          fda.ID,
		ApplicantId: fda.ApplicantId,
		FunctionId:  fda.FunctionId,
		BusinessId:  fda.BusinessId,
		Status:      fda.Status,
	}
}

func (fda *FunctionDataApply) fillApplyFromApplyPO(applyPO *po.FunctionDataApply) {
	fda.ID = applyPO.ID
	fda.ApplicantId = applyPO.ApplicantId
	fda.FunctionId = applyPO.FunctionId
	fda.BusinessId = applyPO.BusinessId
	fda.Status = applyPO.Status
}

func (fda *FunctionDataApply) fillApplyFromApplyResultPO(applyResultPO *po.FunctionDataApplyResult) {
	fda.ID = applyResultPO.ID
	fda.ApplicantId = applyResultPO.ApplicantId
	fda.ApplicantName = applyResultPO.ApplicantName
	fda.FunctionId = applyResultPO.FunctionId
	fda.FunctionName = applyResultPO.FunctionName
	fda.BusinessId = applyResultPO.BusinessId
	fda.BusinessName = applyResultPO.BusinessName
	fda.Status = applyResultPO.Status
}

func (fdal *FunctionDataApplyList) fillApplyListFromApplyResultPOList(applyResultPOList []*po.FunctionDataApplyResult) {
	*fdal = make([]*FunctionDataApply, 0, len(applyResultPOList))
	for _, applyResultPO := range applyResultPOList {
		apply := new(FunctionDataApply)
		apply.fillApplyFromApplyResultPO(applyResultPO)
		*fdal = append(*fdal, apply)
	}
}
