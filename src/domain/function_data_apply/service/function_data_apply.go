package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_data_apply_service/src/domain/function_data_apply/entity"
)

var FunctionDataApplyDomain = new(FunctionDataApply)

type FunctionDataApply struct{}

func (d *FunctionDataApply) CreateApply(apply *entity.FunctionDataApply) *business_error.BusinessError {
	return apply.CreateApply()
}

func (d *FunctionDataApply) GetApplyByID(applyId int64) (*entity.FunctionDataApply, *business_error.BusinessError) {
	apply := new(entity.FunctionDataApply)
	err := apply.GetApplyByID(applyId)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return apply, err
}

func (d *FunctionDataApply) ChangeStatus(apply *entity.FunctionDataApply, status int32) *business_error.BusinessError {
	return apply.ChangeStatus(status)
}

func (d *FunctionDataApply) GetApplyList(applicantId, functionId, businessId *int64, status *int32) ([]*entity.FunctionDataApply, *business_error.BusinessError) {
	applyList := new(entity.FunctionDataApplyList)
	err := applyList.GetApplyList(applicantId, functionId, businessId, status)
	return *applyList, err
}
