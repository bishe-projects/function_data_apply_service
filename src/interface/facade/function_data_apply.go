package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_data_apply_service/src/application/service"
	"gitlab.com/bishe-projects/function_data_apply_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_apply"
)

var FunctionDataApplyFacade = new(FunctionDataApply)

type FunctionDataApply struct{}

func (f *FunctionDataApply) GetApplyList(ctx context.Context, req *function_data_apply.GetApplyListReq) *function_data_apply.GetApplyListResp {
	resp := &function_data_apply.GetApplyListResp{}
	applyList, err := service.FunctionDataApplyApp.GetApplyList(req.ApplicantId, req.FunctionId, req.BusinessId, req.Status)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] get apply list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ApplyList = assembler.ConvertApplyEntityListToApplyList(applyList)
	return resp
}

func (f *FunctionDataApply) CreateApply(ctx context.Context, req *function_data_apply.CreateApplyReq) *function_data_apply.CreateApplyResp {
	resp := &function_data_apply.CreateApplyResp{}
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] create apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.FunctionDataApplyApp.CreateApply(assembler.ConvertCreateApplyReqToApplyEntity(req, uid))
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] create apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *FunctionDataApply) ApproveApply(ctx context.Context, req *function_data_apply.ApproveApplyReq) *function_data_apply.ApproveApplyResp {
	resp := &function_data_apply.ApproveApplyResp{}
	err := service.FunctionDataApplyApp.ApproveApply(ctx, req.ApplyId)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] approve apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *FunctionDataApply) RejectApply(ctx context.Context, req *function_data_apply.RejectApplyReq) *function_data_apply.RejectApplyResp {
	resp := &function_data_apply.RejectApplyResp{}
	err := service.FunctionDataApplyApp.RejectApply(ctx, req.ApplyId)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] reject apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *FunctionDataApply) WithdrawApply(ctx context.Context, req *function_data_apply.WithdrawApplyReq) *function_data_apply.WithdrawApplyResp {
	resp := &function_data_apply.WithdrawApplyResp{}
	err := service.FunctionDataApplyApp.WithdrawApply(ctx, req.ApplyId)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] withdraw apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *FunctionDataApply) AdminApplyList(ctx context.Context, req *function_data_apply.AdminApplyListReq) *function_data_apply.AdminApplyListResp {
	resp := &function_data_apply.AdminApplyListResp{}
	applyList, err := service.FunctionDataApplyApp.AdminApplyList(ctx, req.FunctionId, req.Status)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] get admin apply list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ApplyList = assembler.ConvertApplyEntityListToApplyList(applyList)
	return resp
}

func (f *FunctionDataApply) OwnApplyList(ctx context.Context, req *function_data_apply.OwnApplyListReq) *function_data_apply.OwnApplyListResp {
	resp := &function_data_apply.OwnApplyListResp{}
	applyList, err := service.FunctionDataApplyApp.OwnApplyList(ctx, req.FunctionId, req.Status)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionDataApplyFacade] get own apply list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ApplyList = assembler.ConvertApplyEntityListToApplyList(applyList)
	return resp
}
