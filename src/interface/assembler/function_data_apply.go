package assembler

import (
	"gitlab.com/bishe-projects/common_utils/constant/business_constant"
	"gitlab.com/bishe-projects/function_data_apply_service/src/domain/function_data_apply/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_apply"
)

func ConvertApplyEntityToApply(applyEntity *entity.FunctionDataApply) *function_data_apply.FunctionDataApply {
	return &function_data_apply.FunctionDataApply{
		Id:            applyEntity.ID,
		ApplicantId:   applyEntity.ApplicantId,
		ApplicantName: applyEntity.ApplicantName,
		FunctionId:    applyEntity.FunctionId,
		FunctionName:  applyEntity.FunctionName,
		BusinessId:    applyEntity.BusinessId,
		BusinessName:  applyEntity.BusinessName,
		Status:        applyEntity.Status,
	}
}

func ConvertApplyEntityListToApplyList(applyEntityList []*entity.FunctionDataApply) []*function_data_apply.FunctionDataApply {
	applyList := make([]*function_data_apply.FunctionDataApply, 0, len(applyEntityList))
	for _, applyEntity := range applyEntityList {
		applyList = append(applyList, ConvertApplyEntityToApply(applyEntity))
	}
	return applyList
}

func ConvertCreateApplyReqToApplyEntity(req *function_data_apply.CreateApplyReq, uid int64) *entity.FunctionDataApply {
	return &entity.FunctionDataApply{
		ApplicantId: uid,
		FunctionId:  req.FunctionId,
		BusinessId:  req.BusinessId,
		Status:      business_constant.ApplyStatusApplying,
	}
}
