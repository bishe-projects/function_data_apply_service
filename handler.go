package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/function_data_apply_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_apply"
)

// FunctionDataApplyServiceImpl implements the last service interface defined in the IDL.
type FunctionDataApplyServiceImpl struct{}

// GetApplyList implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) GetApplyList(ctx context.Context, req *function_data_apply.GetApplyListReq) (resp *function_data_apply.GetApplyListResp, err error) {
	klog.CtxInfof(ctx, "GetApplyList req=%+v", req)
	resp = facade.FunctionDataApplyFacade.GetApplyList(ctx, req)
	klog.CtxInfof(ctx, "GetApplyList resp=%+v", resp)
	return
}

// ApproveApply implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) ApproveApply(ctx context.Context, req *function_data_apply.ApproveApplyReq) (resp *function_data_apply.ApproveApplyResp, err error) {
	klog.CtxInfof(ctx, "ApproveApply req=%+v", req)
	resp = facade.FunctionDataApplyFacade.ApproveApply(ctx, req)
	klog.CtxInfof(ctx, "ApproveApply resp=%+v", resp)
	return
}

// RejectApply implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) RejectApply(ctx context.Context, req *function_data_apply.RejectApplyReq) (resp *function_data_apply.RejectApplyResp, err error) {
	klog.CtxInfof(ctx, "RejectApply req=%+v", req)
	resp = facade.FunctionDataApplyFacade.RejectApply(ctx, req)
	klog.CtxInfof(ctx, "RejectApply resp=%+v", resp)
	return
}

// CreateApply implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) CreateApply(ctx context.Context, req *function_data_apply.CreateApplyReq) (resp *function_data_apply.CreateApplyResp, err error) {
	klog.CtxInfof(ctx, "CreateApply req=%+v", req)
	resp = facade.FunctionDataApplyFacade.CreateApply(ctx, req)
	klog.CtxInfof(ctx, "CreateApply resp=%+v", resp)
	return
}

// WithdrawApply implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) WithdrawApply(ctx context.Context, req *function_data_apply.WithdrawApplyReq) (resp *function_data_apply.WithdrawApplyResp, err error) {
	klog.CtxInfof(ctx, "WithdrawApply req=%+v", req)
	resp = facade.FunctionDataApplyFacade.WithdrawApply(ctx, req)
	klog.CtxInfof(ctx, "WithdrawApply resp=%+v", resp)
	return
}

// AdminApplyList implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) AdminApplyList(ctx context.Context, req *function_data_apply.AdminApplyListReq) (resp *function_data_apply.AdminApplyListResp, err error) {
	klog.CtxInfof(ctx, "AdminApplyList req=%+v", req)
	resp = facade.FunctionDataApplyFacade.AdminApplyList(ctx, req)
	klog.CtxInfof(ctx, "AdminApplyList resp=%+v", resp)
	return
}

// OwnApplyList implements the FunctionDataApplyServiceImpl interface.
func (s *FunctionDataApplyServiceImpl) OwnApplyList(ctx context.Context, req *function_data_apply.OwnApplyListReq) (resp *function_data_apply.OwnApplyListResp, err error) {
	klog.CtxInfof(ctx, "OwnApplyList req=%+v", req)
	resp = facade.FunctionDataApplyFacade.OwnApplyList(ctx, req)
	klog.CtxInfof(ctx, "OwnApplyList resp=%+v", resp)
	return
}
