package main

import (
	"github.com/cloudwego/kitex/server"
	function_data_apply "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_apply/functiondataapplyservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8885")
	svr := function_data_apply.NewServer(new(FunctionDataApplyServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
